document.addEventListener('DOMContentLoaded', function(){
	centerImg();
})

function centerImg(){
	var parent = document.getElementById('list-holder'),
		items = Array.prototype.slice.call(parent.children);

	items.forEach(function(element){
		var img = element.querySelector('img'),
			leftMargin = (element.offsetWidth - img.offsetWidth)/2,
			topMargin = (element.offsetHeight - img.offsetHeight)/2;

		img.style.marginTop = topMargin + 'px';
		img.style.marginLeft = leftMargin + 'px';
	});
}