document.addEventListener('DOMContentLoaded', function(){
	addClass();
})

function addClass() {
	// через forEach
	// var parent = document.getElementById('holder'),
	// 	items = Array.prototype.slice.call(parent.children);

	// 	items.forEach(function (element){
	// 		var classList = element.className;

	// 		if(classList.length > 0) {
	// 			classList += ' element';
	// 		} else {
	// 			classList += 'element';
	// 		}

	// 		element.className = classList;
	// 	});

	// через for
	var elements = document.querySelectorAll('#holder > div');

	for(var i=0; i<elements.length; i++) {
		var classList = elements[i].className;

		if(classList.length > 0) {
			classList += ' element';
		} else {
			classList += 'element';
		}

		elements[i].className = classList;
	}
}