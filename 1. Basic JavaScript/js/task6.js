document.addEventListener('DOMContentLoaded', function(){
	showNumber();
})

function showNumber() {
	// вот тут как правильно - найти все ul, а потом в них li, или можно сразу все li ?
	var listElement = document.querySelectorAll('ul > li');

	//через forEach
	listElement.forEach(function(element){

		element.addEventListener('click', function(event){
			event.stopPropagation();

			var target = event.target,
				parent = target.parentNode,
				children = Array.prototype.slice.call(parent.children),
				index = Array.prototype.indexOf.call(children, target) + 1;

			alert('List length = ' + children.length + ' , element index = ' + index);
		});
	});

	//через for
	// for (var i=0; i<listElement.length; i++) {
	// 	var element = listElement[i];

	// 	element.addEventListener('click', function(event){
	// 		event.stopPropagation();

	// 		var target = event.target,
	// 			parent = target.parentNode,
	// 			children = Array.prototype.slice.call(parent.children),
	// 			index = children.indexOf(target) + 1,
	// 			parentListLength = children.length;

	// 		alert('List length = ' + parentListLength + ' , element index = ' + index);
	// 	})
	// }
}