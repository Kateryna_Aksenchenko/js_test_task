document.addEventListener('DOMContentLoaded', function(){
	addImage();
})

function addImage() {
	var holder = document.getElementById('visual'),
		imagesList = holder.innerHTML,
		imageElement = document.createElement('img'),
		imagesListLength,
		index;

	imagesList = imagesList.replace(/\s+/g, '').split(';');

	imagesListLength = imagesList.length;
	index = getRandomNumber(0, imagesList.length - 1);

	imageElement.src = imagesList[index];

	holder.appendChild(imageElement);
}

function getRandomNumber(min, max) {
	return Math.floor((Math.random() * max) + min);
}