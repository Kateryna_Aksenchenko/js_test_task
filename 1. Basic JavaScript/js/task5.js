document.addEventListener('DOMContentLoaded', function(){
	zebra();
})

function zebra() {
	var list = document.querySelectorAll('.list');

	list.forEach(function(element){
		var listElements = element.children;

		for(var i = 0; i < listElements.length; i++) {
			var spanElement = document.createElement('span');
			spanElement.textContent = i + 1;

			listElements[i].insertBefore(spanElement, listElements[i].firstChild);

			if(i % 2 == 0) {
				listElements[i].className = 'alt';
			}
		}
	})
}