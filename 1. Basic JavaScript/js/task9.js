document.addEventListener('DOMContentLoaded', function(){
	slideShow();
})

function slideShow() {
	var slider = document.getElementById('gallery'),
		slides = Array.prototype.slice.call(slider.children),
		slidesAmount = slides.length,
		playButton = document.getElementById('btn');

		var isTimeoutRunning;

	function hideSlides(){
		slides.forEach(function(element){
			element.style.zIndex = 0;
		})
	}

	hideSlides();

	function showCurrentSlide(index){
		slides[index].style.zIndex = 1;
		slides[index].className = 'current';
	}

	showCurrentSlide(0);

	playButton.addEventListener('click', function(e){
		e.preventDefault();

		if(isTimeoutRunning) {
			clearTimeout(isTimeoutRunning);
			isTimeoutRunning = null;
			playButton.textContent = 'PLAY';
		} else {
			play(0);
			playButton.textContent = 'STOP';
		}
	});

	function play(startSlide){
		var index = startSlide;

		isTimeoutRunning = setTimeout(function run(){

			if(index <= slidesAmount - 1) {
				slides[index].style.zIndex = 1;
				index++;
			} else {
				index = 0;

				hideSlides();
			}
			isTimeoutRunning = setTimeout(run, 500);
		}, 500);
	}
}