document.addEventListener('DOMContentLoaded', function(){
	openClose();
})

function openClose() {
	var button = document.querySelectorAll('.button');

	button.forEach(function(element){
		element.addEventListener('click', function(e){
			e.preventDefault();

			var contentBlock = this.parentNode.querySelector('.box');
			if(this.classList.contains('active')){
				this.classList.remove('active');
				contentBlock.style.display = 'none';
			} else {
				this.classList.add('active');
				contentBlock.style.display = 'block';
			}
		})
	})
}