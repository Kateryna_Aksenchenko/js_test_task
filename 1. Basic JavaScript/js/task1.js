document.addEventListener('DOMContentLoaded', function(){
	equalColumnsHeight('#block1, #block2');
	equalColumnsHeight('#list-holder');
	equalColumnsHeight('.element, .container, .my-box');
});

function equalColumnsHeight(){
	if(!arguments[0]) return;

	var arg = (arguments[0]).split(', '),
		blocks = [];

	if(arg.length > 1) {
		arg.forEach(function(element){
			blocks.push(document.querySelector(element));
		})
	} else if (arg.length === 1) { //case with parent element
		var tempArr = Array.prototype.slice.call(document.querySelector(arg[0]).children);
		tempArr.forEach(function(element){
			blocks.push(element);
		});
	}

	setColumnsHeight(blocks);
}

function getMaxHeight(array){
	var max = 0;

	arguments[0].forEach(function(element) {
		if(element.offsetHeight > max) {
			max = element.offsetHeight;
		}
	});

	return max;
}

function setColumnsHeight(array){
	var max = getMaxHeight(arguments[0]);

	arguments[0].forEach(function(element) {
		var innerHeight = parseInt(getComputedStyle(element).height),
			delta = element.offsetHeight - innerHeight;

		if(delta <= 0) {
			element.style.height = max + 'px';
		} else {
			element.style.height = max - delta + 'px';
		}
	});
}