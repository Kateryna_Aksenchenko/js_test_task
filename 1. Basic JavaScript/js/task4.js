document.addEventListener('DOMContentLoaded', function(){
	addClass();
})

function addClass() {
	var list = document.querySelectorAll('.list');

	list.forEach(function(element){
		element.children[0].className = 'first';
		element.children[element.children.length - 1].className = 'last';
	});

	// или такой вариант ?
	// var firstElement = document.querySelectorAll('.list > li:first-child'),
	// 	lastElement = document.querySelectorAll('.list > li:last-child');

	// firstElement.forEach(function(element){
	// 	element.className = 'first';
	// });

	// lastElement.forEach(function(element){
	// 	element.className = 'last';
	// });
}