jQuery(function() {
	openClose();
	accordion();
});

function openClose() {
	var openCloseBlock = $('.open-close'),
		opener = $('.opener'),
		slide = $('.slide');

	var hideAllSlides = function () {
		$(slide).hide();
	}

	var showSlide = function (slide) {
		$(slide).slideDown();
	}

	var hideSlide = function (slide) {
		$(slide).slideUp();
	}

	$(opener).each(function (i, el) {
		$(el).on('click', function (e) {
			e.preventDefault();

			var parentEl = $(this).parent(openCloseBlock),
				slideEl = parentEl.find(slide);

			if ($(parentEl).hasClass('active')) {
				$(parentEl).removeClass('active');
				hideSlide(slideEl);
			} else {
				$(parentEl).addClass('active');
				showSlide(slideEl);
			}
		})
	});

	hideAllSlides();
}

function accordion() {
	var accordion = $('.accordion'),
		opener = $('a'),
		slide = $(accordion).find('ul');

	var hideAllSlidesOnPageLoad = function () {
		if (!$(slide).parent('li').hasClass('active')) {
			$(slide).hide();
		}
	}

	var showSlide = function (slide) {
		$(slide).slideDown();
	}

	var hideSlide = function (slide) {
		$(slide).slideUp();
	}

	$(opener).each(function (i, el) {
		$(el).on('click', function (e) {
			e.preventDefault();

			var parentEl = $(this).parent('li'),
				slideEl = parentEl.children('ul');

			if ($(parentEl).hasClass('active')) {
				$(parentEl).removeClass('active');
				hideSlide(slideEl);
			} else {
				$(parentEl).addClass('active');
				showSlide(slideEl);
			}
		})
	});

	hideAllSlidesOnPageLoad();
}

//попробовать переписать как плагин
//добавить проверки ?