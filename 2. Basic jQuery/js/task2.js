jQuery(function() {
	clearInput();
});

function clearInput() {
	var field = $('input[type="text"], textarea'),
		startValue;

	var getValue = function (element) {
		var value = $(element).val();

		return value;
	}

	var clearField = function (element) {
		$(element).val('');
	}

	$(field).on('focus', function () {
		startValue = getValue($(this));

		clearField($(this));
	}).on('blur', function () {
		var newValue = getValue($(this));

		if(!newValue) {
			$(this).val(startValue);
		}
	});
}