jQuery(function() {
	showBlock();
});

function showBlock() {
	var field = $('input[type=radio]'),
		holder = $('.rholder');

	var hideBlock = function () {
		$(holder).hide();
	}

	var showBlock = function (element) {
		$(element).show();
	}

	var checkOnLoad = function () {
		$(holder).hide();

		$(field).each(function (index, element) {
			if($(element).prop("checked")) {
				var block = $(this).parent().find(holder);

				showBlock(block);
			}
		})
	}

	checkOnLoad();

	$(field).on('click', function () {
		hideBlock();

		var block = $(this).parent().find(holder);

		showBlock(block);
	})
}