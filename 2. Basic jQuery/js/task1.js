jQuery(function() {
	toggleClass();
});

function toggleClass() {
	var btn = $('.button');

	if($(btn).length){
		$(btn).on('click', function(e){
			e.preventDefault();

			var box = $(this).parents('.box'),
				element = $(box).find('.element');

			if($(box).hasClass('opened')){
				$(box).removeClass('opened');
				$(element).css({
					"background": "none"
				})
			} else {
				$(box).addClass('opened');
				$(element).css({
					"background": "red"
				})
			}
		})
	}
}