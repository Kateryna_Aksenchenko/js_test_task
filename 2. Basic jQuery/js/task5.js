jQuery(function() {
	moveTank();
});

function moveTank () {
	var tank = $('.box'),
		tankSize = {width: $(tank).outerWidth(), height: $(tank).outerHeight()},
		tankPosition = {left: 0, top: 0},
		holder = $('.box-holder'),
		holderSize = {width: $(holder).outerWidth(), height: $(holder).outerHeight()};

	var setPosition = function (left, top) {
		$(tank).css({
			'left': left||tankPosition.left,
			'top': top||tankPosition.top
		});
	}

	var move = {
		left: function () {
			tankPosition.left -= 1;

			$(tank).css({
				'left': tankPosition.left,
				'background-image': 'url(images/tank-left.gif)'
			});
		},
		up: function () {
			tankPosition.top -= 1;

			$(tank).css({
				'top': tankPosition.top,
				'background-image': 'url(images/tank-up.gif)'
			});
		},
		right: function () {
			tankPosition.left += 1;

			$(tank).css({
				'left': tankPosition.left,
				'background-image': 'url(images/tank-right.gif)'
			});
		},
		down: function () {
			tankPosition.top += 1;

			$(tank).css({
				'top': tankPosition.top,
				'background-image': 'url(images/tank-down.gif)'
			});
		}
	};

	var resetTankPosition = function () {
		tankPosition.left = (holderSize.width - tankSize.width) / 2;
		tankPosition.top = (holderSize.height - tankSize.height) / 2;

		setPosition(tankPosition.left, tankPosition.top);

		return tankPosition;
	}

	var moveTank = function () {
		//move with arrow key
		$(document).keydown(function(event){
			event.preventDefault();

			var eventCode = event.keyCode;

			if (eventCode == 37 && tankPosition.left > 0) {
				move.left();
			} else if (eventCode == 38 && tankPosition.top > 0) {
				move.up();
			} else if (eventCode == 39 && tankPosition.left < holderSize.width) {
				move.right();
			} else if (eventCode == 40 && tankPosition.top < holderSize.height) {
				move.down();
			}

			console.log('left ' + tankPosition.left + ' top ' + tankPosition.top)
		});

		//move with mouse click
		$(holder).mousedown(function(e){
			var holderPosition = $(this).offset(),
				relativeX = e.pageX - holderPosition.left,
				relativeY = e.pageY - holderPosition.top;

			if (relativeX < tankSize.width) {
				tankPosition.left = relativeX;
			} else if (relativeX > (holderSize.width - tankSize.width)){
				tankPosition.left = relativeX - tankSize.width;
			} else {
				tankPosition.left = relativeX - tankSize.width / 2;
			}

			if (relativeY < tankSize.height) {
				tankPosition.top = relativeY
			} else if (relativeY > (holderSize.height - tankSize.height)) {
				tankPosition.top = relativeY - tankSize.height;
			} else {
				tankPosition.top = relativeY - tankSize.height / 2;
			}

			setPosition(tankPosition.left, tankPosition.top);
		});
	}

	resetTankPosition();
	moveTank();
}