jQuery(function() {
	showTooltip();
});

function showTooltip () {
	var links = $('a[title]'),
		tooltip = $('<div id="tooltip"></div>'),
		winHeight = $(window).height(),
		tooltipText = [];

	var getPosition = function () {
		var coords = {
			x: event.pageX,
			y: event.pageY
		}

		return coords;
	}

	var setPosition = function (coords, marginTop) {
		tooltip.css({
			'left': coords.x,
			'top': coords.y,
			'margin-top': '10px'
		})
	}

	$('body').prepend(tooltip);
	$(tooltip).hide();

	$(links).each(function (index,element) {
		tooltipText.push($(this).prop('title'));
		$(this).prop('title', '');

		$(this).on('mouseenter', function () {
			$(tooltip).text(tooltipText[index]).show();
			var coords = getPosition();
			setPosition(coords);
		})

		$(this).on('mouseleave', function () {
			$(tooltip).text('').hide();
			var coords = getPosition();
			setPosition(coords);
		});

		$(this).on('mousemove', function () {
			var coords = getPosition();
			setPosition(coords);
		});
	})
}